#!/usr/bin/env python
import argparse
import io
import os
import random
import sys
import time
from collections import namedtuple
from contextlib import contextmanager
from datetime import datetime
from PIL import Image
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def chrome_options(*, headless=False, chrome_bin=None):
    options = Options()
    options.binary_location = chrome_bin
    options.add_argument("--disable-geolocation")
    options.add_argument("--disable-notifications")
    options.add_argument("--window-size=1280,1600")
    if headless:
        options.add_argument("--headless")
    return options

@contextmanager
def load_driver(*, headless=False, **kwargs):
    driver = webdriver.Chrome(executable_path=os.path.abspath('chromedriver'),
                              chrome_options=chrome_options(headless=headless,
                                                            **kwargs))

    yield driver

    if not headless:
        # pause to check
        input()

    driver.close()

def login(driver, uid, pwd):
    driver.get('https://www.facebook.com/')

    email_field = driver.find_element_by_id('email')
    email_field.clear()
    email_field.send_keys(uid)

    pass_field = driver.find_element_by_id('pass')
    pass_field.clear()
    pass_field.send_keys(pwd)

    driver.find_element_by_id('loginbutton').click()

Friend = namedtuple('Friend', 'link name')

def get_friends(driver, name):
    driver.get('https://www.facebook.com/{}/friends_with_unseen_posts'
               .format(name))

    if 'Friends with new posts' not in driver.page_source:
        return []

    seen = set()
    links = []
    # get links to all friends' pages (selector matches two links per friend)
    for el in driver.find_elements_by_css_selector(
            'div[data-testid="friend_list_item"] '
            'a[data-hovercard]'):
        link = el.get_attribute('href')
        if el.text:
            seen.add(link)
            links.append(Friend(link, el.text))

    return links

def get_posts(driver, page):
    driver.get(page)

    # TODO: inject javascript to hide junk from page first?

    return driver.find_elements_by_css_selector(
        'div[data-pnref="unseen-section"] div[role="article"][id^="tl_unit_"]')

def get_post_links(post):
    seen = set()
    links = []
    for el in post.find_elements_by_tag_name('a'):
        link = el.get_attribute('href')
        if link not in seen:
            links.append(link)
            seen.add(link)
    return links

def get_post_link(links, base):
    for link in links:
        if link.startswith(base + '/posts/') and not '?comment_id=' in link:
            return link

    for link in links:
        if '/photo.php' in link or '/video.php' in link:
            return link

def interesting_links(links, base):
    return [link for link in links
            if not ('/ufi/' in link
                    or '/l.php' in link
                    or '?comment_id=' in link
                    or link.startswith(base + '?'))]

def screenshot_post(driver, post, filename):
    # scroll to post manually so it's not hidden by toolbar, which
    # location_once_scrolled_into_view may do...
    location = post.location
    pad_y = 100
    if location['y'] > pad_y:
        driver.execute_script('window.scrollTo(0, {})'
                              .format(location['y'] - pad_y))
        y = pad_y
    else:
        y = location['y']

    size = post.size
    left = location['x']
    top = y
    right = left + size['width']
    bottom = top + size['height']

    png = driver.get_screenshot_as_png()
    img = Image.open(io.BytesIO(png))
    img = img.crop((left, top, right, min(bottom, img.size[1])))
    img.save(filename)

def make_output(timestamp):
    os.mkdir('output_' + timestamp)

    # can't overwrite symlink, so create a new one and rename
    os.symlink('output_' + timestamp, 'output_tmp')
    os.replace('output_tmp', 'output')

def user_header(friend):
    return ('<thead><tr><td colspan="3">'
            '<h1><a href="{link}">{name}</a></h1>'
            '</td></tr></thead><tbody><tr>'
            .format(link=friend.link, name=friend.name))

def post_td(post_id, links, base, post_png):
    link = get_post_link(links, base)
    print(link)

    return ('''<td>
                   <div><a href="{link}">{link}</a>{menu}</div>
                   <img src="{png}" />
               </td>'''
            .format(link=link,
                    menu=link_menu(post_id + '-links', links, base),
                    png=post_png))

def link_menu(ul_id, links, base):
    return (
        '<a href="javascript:void({click})">&#9776;</a>'
        '<ul id="{ul_id}" style="display: none;">{links}</ul>'
        .format(click="document.getElementById('{}').style=''".format(ul_id),
                ul_id=ul_id,
                links=''.join('<li><a href="{}">{}</a></li>'
                              .format(link, link)
                              for link in interesting_links(links, base))))

def browse(*, uid=None, pwd=None, name=None, delay=True, **kwargs):
    with load_driver(**kwargs) as driver:

        login(driver, uid, pwd)

        timestamp = datetime.now()
        make_output(timestamp.strftime('%Y%m%d%H%M%S'))

        with open('output/summary.html', 'w') as summary:
            summary.write('''<html><head><title>Summary</title>
                             <style>
                               h1 {
                                 font-family: sans-serif;
                                 margin-top: 1em;
                               }
                               table tbody td {
                                 vertical-align: top;
                               }
                             </style>
                             </head><body><table>''')

            for i, friend in enumerate(get_friends(driver, name)):
                print(friend.name)
                summary.write(user_header(friend))

                for j, post in enumerate(get_posts(driver, friend.link)):
                    post_id = 'friend{}-post{}'.format(i, j)
                    screenshot_post(driver, post,
                                    'output/{}.png'.format(post_id))
                    summary.write(post_td(post_id, get_post_links(post),
                                          friend.link.split('?')[0],
                                          post_id + '.png'))

                summary.write('</tr></tbody>')

                # random delay between requests to try and pretend to be human
                if delay:
                    time.sleep(random.uniform(1.0, 3.0))

            summary.write('</table></body></html>')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--headless', action='store_true')
    parser.add_argument('--fast', action='store_true')
    args = parser.parse_args()

    for key, desc in [('CHROME_BIN', 'path to chrome binary'),
                      ('FB_UID', 'your email address'),
                      ('FB_PWD', 'your password'),
                      ('FB_NAME', 'your facebook user name (in url)')]:
        if key not in os.environ:
            print('must export {}="{}"'.format(key, desc))
            sys.exit()

    browse(uid=os.environ['FB_UID'],
           pwd=os.environ['FB_PWD'],
           name=os.environ['FB_NAME'],
           chrome_bin=os.environ['CHROME_BIN'],
           headless=args.headless,
           delay=not args.fast)
