# About

`manatee` will browse facebook and screenshot new posts from each of your
friends and put them into a single summary page:

* see everything your friends post
* no intermitten reinforcement
* no recommended posts or ads

# Installation

1. Install (preferrably in virtualenv)

    pip install -r requirements.txt

2. Download [https://sites.google.com/a/chromium.org/chromedriver/](chromedriver)
and copy to this directory.

3. Create `env.sh`:

    export FB_UID="..."  # email
    export FB_PWD="..."  # password
    export FB_NAME="..."  # username from your profile page url
    export CHROME_BIN="..."  # path to chrome binary

# Usage

1. Run

    source env.sh
    ./manatee.py --headless  # run without headless to watch it go

2. Browse to `./output/summary.html`

3. Wait until tomorrow and do it again
